# Deep universal react redux Boilerplate

This boilerplate is made to help me to bootstrap react redux project. It may change overtime.

# Technologies

* React
* Redux
* Next.js
* Immutable

# Tooling

* Jest
* Enzyme
* Flow
* lint-stage
* prettier
* husky

# How to use

Work in progress

# Credit

* Mathieu Nivoliez <mathieu.nivoliez@laposte.net>