import { createStore, compose, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";

import DevTools from "../containers/DevTools";

import reducer from "./modules/reducer";

const enhancers = compose(
  applyMiddleware(thunkMiddleware),
  typeof window !== "undefined" && process.env.NODE_ENV !== "production"
    ? DevTools.instrument()
    : f => f
);

export const startClock = () =>
  dispatch => {
    return setInterval(
      () => dispatch({ type: "TICK", light: true, ts: Date.now() }),
      800
    );
  };

export const initStore = initialState => {
  return createStore(reducer, initialState, enhancers);
};
