import { combineReducers } from "redux";
import merge from "lodash/merge";

const initialState = {};

// Updates an entity cache in response to any action with response.entities.
const entities = (state = initialState, action) => {
  if (action.entities) {
    return merge({}, state, action.entities);
  }

  return state;
};

const ticks = (state = { lastUpdate: 0, light: false }, action) => {
  switch (action.type) {
    case "TICK":
      return { lastUpdate: action.ts, light: !!action.light };
    default:
      return state;
  }
};

export default combineReducers({
  entities,
  ticks
});
