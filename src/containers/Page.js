import Link from "next/link";
import Head from "next/head";
import { connect } from "react-redux";
import Clock from "../components/Clock";
import DevTools from "./DevTools";

export default connect(state => state)(({
  title,
  linkTo,
  lastUpdate,
  light
}) => {
  return (
    <div>
      <Head><title>{title}</title></Head>
      <h1>{title}</h1>
      <Clock lastUpdate={lastUpdate} light={light} />
      <nav>
        <Link href={linkTo}><a>Navigate</a></Link>
      </nav>
      <DevTools />
    </div>
  );
});
