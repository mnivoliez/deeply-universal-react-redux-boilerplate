import React from "react";
import { initStore, startClock } from "../redux/store";
import withRedux from "next-redux-wrapper";
import Page from "../containers/Page";

class Counter extends React.Component {
  static getInitialProps({ store, isServer }) {
    store.dispatch({ type: "TICK", light: !isServer, ts: Date.now() });
    return { isServer };
  }

  componentDidMount() {
    this.timer = this.props.dispatch(startClock());
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    return <Page title="Index Page" linkTo="/other" />;
  }
}

export default withRedux(initStore)(Counter);
