import Document, { Head, Main, NextScript } from "next/document";

export default class extends Document {
  static async getInitialProps({ renderPage }) {
    // see https://github.com/nfl/react-helmet#server-usage for more information
    // 'head' was occupied by 'renderPage().head', we cannot use it
    return { ...renderPage() };
  }

  render() {
    return (
      <html>
        <Head>
          <title>Website</title>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
